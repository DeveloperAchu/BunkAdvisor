package com.titans.bunkadvisor;

import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;
import com.titans.bunkadvisor.model.Attendance;
import com.titans.bunkadvisor.model.AttendanceDao;
import com.titans.bunkadvisor.model.DaoMaster;
import com.titans.bunkadvisor.model.DaoSession;
import com.titans.bunkadvisor.model.Subjects;
import com.titans.bunkadvisor.model.SubjectsDao;

import java.util.List;

public class ActivitySubjectDetail extends Fragment {

    DaoMaster daoMaster;
    DaoMaster.DevOpenHelper helper;
    SQLiteDatabase db;
    DaoSession daoSession;
    SubjectsDao subjectsDao;
    AttendanceDao attendanceDao;
    List<Attendance> attendanceList;
    View view, promptsView;
    AlertDialog alertDialog;
    ColorPicker picker;
    SaveItem saveItem;

    String string, getString, formattedString;
    LayoutInflater layoutInflater, layoutInflaterEdit;
    CardView cardView;
    TextView subjectName, currentPercentage, safeBunks, passPercentage, attended, bunked, total, newSelectedColor;
    EditText newSubjectName, newPassPercentage;

    Button attendedButton, bunkedButton;
    ImageButton moreButton;
    Subjects selectedSubject;
    long selectedSubjectID;
    String subName, passPer;

    FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.layoutInflater = inflater;
        this.view = container;
        return inflater.inflate(R.layout.subject_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            saveItem = bundle.getParcelable("data");
        }

        try {
            if (!saveItem.isTabletUI())
                saveItem.setListFragment(false);
            saveItem.setAttendanceFragment(false);
            ((MainActivity) getActivity()).fab.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }

        saveItem.setAllAttendance(true);
        saveItem.setAttendedOnly(false);

        fragmentTransaction = getFragmentManager().beginTransaction();

        Context context = getContext();
        helper = new DaoMaster.DevOpenHelper(context, "bunk-db", null);
        db = helper.getReadableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        subjectsDao = daoSession.getSubjectsDao();
        attendanceDao = daoSession.getAttendanceDao();

        try {
            selectedSubject = subjectsDao.load(saveItem.getId());
            selectedSubjectID = saveItem.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }

        cardView = (CardView) view.findViewById(R.id.card_details);
        subjectName = (TextView) view.findViewById(R.id.details_sub_name);
        currentPercentage = (TextView) view.findViewById(R.id.details_cur_per);
        safeBunks = (TextView) view.findViewById(R.id.details_safe_bunks);
        passPercentage = (TextView) view.findViewById(R.id.details_pass_per);
        attended = (TextView) view.findViewById(R.id.details_attended);
        bunked = (TextView) view.findViewById(R.id.details_bunked);
        total = (TextView) view.findViewById(R.id.details_total);

        attendedButton = (Button) view.findViewById(R.id.details_attended_button);
        bunkedButton = (Button) view.findViewById(R.id.details_bunked_button);
        moreButton = (ImageButton) view.findViewById(R.id.more_button);

        attendedButton.setTextColor(ContextCompat.getColor(context, R.color.greenColoredButton));
        bunkedButton.setTextColor(ContextCompat.getColor(context, R.color.redColoredButton));

        attendedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attendanceButtonClicked(getResources().getString(R.string.status_attended));
            }
        });

        bunkedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attendanceButtonClicked(getResources().getString(R.string.status_bunked));
            }
        });

        moreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getContext(), v);
                popupMenu.inflate(R.menu.popup);
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals(getResources().getString(R.string.history))) {
                            saveItem.setId(selectedSubject.getId());
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

                            if (saveItem.isTabletUI()) {
                                Fragment subjectDetailFragment = new ActivitySubjectDetail();
                                fragmentTransaction.replace(R.id.subject_list, subjectDetailFragment);
                                Bundle bundleDetail = new Bundle();
                                bundleDetail.putParcelable("data", saveItem);
                                subjectDetailFragment.setArguments(bundleDetail);

                                Fragment attendanceFragment = new ActivityAttendance();
                                fragmentTransaction.replace(R.id.subject_details, attendanceFragment);
                                Bundle bundleAttendance = new Bundle();
                                bundleAttendance.putParcelable("data", saveItem);
                                attendanceFragment.setArguments(bundleAttendance);

                            } else {
                                Fragment attendanceFragment = new ActivityAttendance();
                                fragmentTransaction.replace(R.id.phone_layout, attendanceFragment);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("data", saveItem);
                                attendanceFragment.setArguments(bundle);
                            }

                            fragmentTransaction.commit();

                        } else if (item.getTitle().equals(getResources().getString(R.string.change_theme))) {
                            editClicked();
                        } else if (item.getTitle().equals(getResources().getString(R.string.delete))) {
                            deleteClicked();
                        }

                        return true;
                    }
                });
            }
        });

        try {
            cardView.setVisibility(View.VISIBLE);
            subjectName.setTextColor(selectedSubject.getTheme());
            if (!saveItem.isColorSelected()) {
                saveItem.setNewColor(selectedSubject.getTheme());
            }
            subjectName.setText(selectedSubject.getSub_name());
            subName = selectedSubject.getSub_name();
            subjectName.setShadowLayer(1.6f, 1.5f, 1.3f, ContextCompat.getColor(context, R.color.colorPrimaryText));

            String str;
            int len;
            SpannableStringBuilder spannableStringBuilder;
            try {
                str = String.valueOf(selectedSubject.getCur_per().toString().substring(0, 5)) + " %";
                len = 5;
            } catch (Exception e) {
                try {
                    str = String.valueOf(selectedSubject.getCur_per().toString().substring(0, 4)) + " %";
                    len = 4;
                } catch (Exception e1) {
                    str = String.valueOf(selectedSubject.getCur_per().toString().substring(0, 3)) + " %";
                    len = 3;
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }

            spannableStringBuilder = new SpannableStringBuilder(str);

            if (selectedSubject.getCur_per() < selectedSubject.getPass_per()) {
                spannableStringBuilder.setSpan(
                        new ForegroundColorSpan(ContextCompat.getColor(context, R.color.redColor)),
                        0,
                        len,
                        Spanned.SPAN_INCLUSIVE_INCLUSIVE
                );
            } else {
                spannableStringBuilder.setSpan(
                        new ForegroundColorSpan(ContextCompat.getColor(context, R.color.greenColor)),
                        0,
                        len,
                        Spanned.SPAN_INCLUSIVE_INCLUSIVE
                );
            }

            currentPercentage.setText(spannableStringBuilder);

            int safeBunksInDB = selectedSubject.getSafe_bunks();
            int length;
            if (safeBunksInDB < 0) {
                length = selectedSubject.getSafe_bunks().toString().length();
                getString = context.getResources().getString(R.string.attend_classes);
                formattedString = String.format(getString, (safeBunksInDB * -1), selectedSubject.getPass_per());
                formattedString = formattedString + " %";

                spannableStringBuilder = new SpannableStringBuilder(formattedString);
                spannableStringBuilder.setSpan(
                        new StyleSpan(Typeface.BOLD),
                        19,
                        19 + length - 1,
                        Spanned.SPAN_INCLUSIVE_INCLUSIVE
                );
            } else {
                getString = context.getResources().getString(R.string.safe_bunks);
                formattedString = String.format(getString, safeBunksInDB);
                length = getString.length();
                spannableStringBuilder = new SpannableStringBuilder(formattedString);
                spannableStringBuilder.setSpan(
                        new StyleSpan(Typeface.BOLD),
                        21,
                        length - 1,
                        Spanned.SPAN_INCLUSIVE_INCLUSIVE
                );
            }

            safeBunks.setText(spannableStringBuilder);

            string = context.getResources().getString(R.string.details_pass_per);
            passPercentage.setText(String.format(string, selectedSubject.getPass_per()));
            passPer = String.valueOf(selectedSubject.getPass_per());

            string = context.getResources().getString(R.string.details_attended);
            attended.setText(String.format(string, selectedSubject.getAttend()));

            string = context.getResources().getString(R.string.details_bunked);
            bunked.setText(String.format(string, selectedSubject.getBunk()));

            string = context.getResources().getString(R.string.details_total);
            total.setText(String.format(string, selectedSubject.getTotal()));
        } catch (Exception e) {
            e.printStackTrace();
            cardView.setVisibility(View.GONE);
        }

        if (saveItem.isDeletePopup() && saveItem.isScreenRotated()) {
            saveItem.setScreenRotated(false);
            deleteClicked();
        }

        if (saveItem.isEditPopup() && saveItem.isScreenRotated()) {
            saveItem.setScreenRotated(false);
            editClicked();
        }

        if (saveItem.isEditColorPicker() && saveItem.isScreenRotated()) {
            saveItem.setScreenRotated(false);
            colorPicker();
        }
    }

    private void editClicked() {
        layoutInflaterEdit = LayoutInflater.from(getContext());
        promptsView = layoutInflaterEdit.inflate(R.layout.popup, null);
        saveItem.setEditPopup(true);

        newSubjectName = (EditText) promptsView.findViewById(R.id.subject_name);
        newPassPercentage = (EditText) promptsView.findViewById(R.id.pass_per);
        newSelectedColor = (TextView) promptsView.findViewById(R.id.selected_color);

        if (saveItem.getEditSubName() != null) {
            subName = saveItem.getEditSubName();
        }

        if (saveItem.getEditPassPer() != null)
            passPer = saveItem.getEditPassPer();

        try {
            newSubjectName.setText(subName);
            newPassPercentage.setText(passPer);
            newSelectedColor.setBackgroundColor(saveItem.getNewColor());
        } catch (Exception e) {
            e.printStackTrace();
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView)
                .setTitle(getResources().getString(R.string.edit))
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                subName = newSubjectName.getText().toString();
                                passPer = newPassPercentage.getText().toString();
                                if (subName == null || subName.equals("") || passPer == null || passPer.equals("")) {
                                    Toast.makeText(getContext(), "Enter all the fields", Toast.LENGTH_SHORT).show();
                                    layoutInflaterEdit = null;
                                    promptsView = null;
                                    editClicked();
                                }
                                else {
                                    Subjects updatedSubject = selectedSubject;
                                    updatedSubject.setSub_name(subName);
                                    updatedSubject.setPass_per(Float.parseFloat(passPer));
                                    updatedSubject.setTheme(saveItem.getNewColor());
                                    subjectsDao.delete(selectedSubject);
                                    subjectsDao.insert(updatedSubject);
                                    selectedSubject = updatedSubject;

                                    reCalculate();

                                    layoutInflaterEdit = null;
                                    promptsView = null;
                                    saveItem.setEditPopup(false);
                                    saveItem.setEditColorPicker(false);
                                    fragmentTransaction();

                                    setToNull();
                                }
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                layoutInflaterEdit = null;
                                promptsView = null;
                                saveItem.setEditPopup(false);
                                saveItem.setEditColorPicker(false);
                                setToNull();
                                alertDialog.dismiss();
                            }
                        });

        try {
            alertDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        TextView colorPickerButton = (TextView) promptsView.findViewById(R.id.selected_color);
        colorPickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(promptsView.getWindowToken(), 0);
                layoutInflaterEdit = null;
                promptsView = null;
                saveItem.setEditPopup(false);
                saveItem.setEditColorPicker(true);
                alertDialog.dismiss();
                subName = newSubjectName.getText().toString();
                passPer = newPassPercentage.getText().toString();
                colorPicker();
            }
        });

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.greenColoredButton));

        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(ContextCompat.getColor(getContext(), R.color.redColoredButton));


    }

    private void reCalculate() {

        int attendedClasses = selectedSubject.getAttend();
        int totalClasses = selectedSubject.getTotal();
        if (totalClasses == 0)
            return;
        float newPercentage = calculatePercentage(attendedClasses, totalClasses);
        float passPercentage = selectedSubject.getPass_per();
        int newSafeBunks = findSafeBunks(newPercentage, passPercentage, attendedClasses, totalClasses);

        Subjects updatedSubject = selectedSubject;
        updatedSubject.setCur_per(newPercentage);
        updatedSubject.setSafe_bunks(newSafeBunks);

        subjectsDao.delete(selectedSubject);
        subjectsDao.insert(updatedSubject);
        selectedSubject = updatedSubject;

    }

    private void setToNull() {
        saveItem.setEditSubName(null);
        saveItem.setEditPassPer(null);
        saveItem.setColorSelected(false);
    }

    private void colorPicker() {
        saveItem.setColorSelected(false);
        saveItem.setEditColorPicker(true);
        layoutInflaterEdit = LayoutInflater.from(getContext());
        promptsView = layoutInflaterEdit.inflate(R.layout.color_picker, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                layoutInflaterEdit = null;
                                promptsView = null;
                                if (picker.getColor() == 0 || picker.getColor() == -1)
                                    saveItem.setNewColor(-1447447);
                                else
                                    saveItem.setNewColor(picker.getColor());

                                saveItem.setColorSelected(true);
                                saveItem.setEditColorPicker(false);
                                saveItem.setEditPopup(true);
                                editClicked();
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                layoutInflaterEdit = null;
                                promptsView = null;
                                saveItem.setEditColorPicker(false);
                                saveItem.setEditPopup(true);
                                editClicked();
                            }
                        });

        try {
            alertDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        picker = (ColorPicker) promptsView.findViewById(R.id.picker);
        SVBar svBar = (SVBar) promptsView.findViewById(R.id.sv_bar);
        OpacityBar opacityBar = (OpacityBar) promptsView.findViewById(R.id.opacity_bar);
        SaturationBar saturationBar = (SaturationBar) promptsView.findViewById(R.id.saturation_bar);
        ValueBar valueBar = (ValueBar) promptsView.findViewById(R.id.value_bar);

        picker.addSVBar(svBar);
        picker.addOpacityBar(opacityBar);
        picker.addSaturationBar(saturationBar);
        picker.addValueBar(valueBar);

        picker.getColor();

        picker.setOldCenterColor(saveItem.getNewColor());

        picker.setShowOldCenterColor(false);

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.greenColoredButton));

        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(ContextCompat.getColor(getContext(), R.color.redColoredButton));

    }

    private void deleteClicked() {
        saveItem.setDeletePopup(true);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("Are you sure to delete this subject?")
                .setCancelable(false)
                .setPositiveButton("DELETE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                subjectsDao.deleteByKey(selectedSubjectID);
                                attendanceList = attendanceDao.queryBuilder()
                                        .where(AttendanceDao.Properties.Subjects_id.eq(selectedSubjectID))
                                        .list();
                                deleteAttendanceHistory();

                                if (saveItem.isTabletUI()) {
                                    saveItem.setPosition(0);
                                    Fragment subjectListFragment = new ActivitySubjectList();
                                    fragmentTransaction.replace(R.id.subject_list, subjectListFragment);
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("data", saveItem);
                                    subjectListFragment.setArguments(bundle);

                                    Fragment subjectDetailFragment = new ActivitySubjectDetail();
                                    fragmentTransaction.replace(R.id.subject_details, subjectDetailFragment);
                                    Bundle bundleDetail = new Bundle();
                                    bundleDetail.putParcelable("data", saveItem);
                                    subjectDetailFragment.setArguments(bundleDetail);

                                } else {
                                    Fragment subjectListFragment = new ActivitySubjectList();
                                    fragmentTransaction.replace(R.id.phone_layout, subjectListFragment);
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("data", saveItem);
                                    subjectListFragment.setArguments(bundle);
                                }

                                fragmentTransaction.commit();
                                layoutInflaterEdit = null;
                                promptsView = null;
                                saveItem.setDeletePopup(false);
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                layoutInflaterEdit = null;
                                promptsView = null;
                                saveItem.setDeletePopup(false);
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(ContextCompat.getColor(getContext(), R.color.redColoredButton));

        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(ContextCompat.getColor(getContext(), R.color.greenColoredButton));
    }

    private void deleteAttendanceHistory() {

        for (int i = 0; i < attendanceList.size(); i++) {
            attendanceDao.deleteByKey(attendanceList.get(i).getId());
        }

    }

    private void attendanceButtonClicked(String status) {
        String date = ((MainActivity) getActivity()).datePickerButton.getText().toString();
        Subjects clickedSubject = selectedSubject;
        String actualDate;
        if (saveItem.getMonth() < 10) {
            if (saveItem.getDay() < 10) {
                actualDate = "" + saveItem.getYear() + "0" + saveItem.getMonth() + "0" + saveItem.getDay();
            } else {
                actualDate = "" + saveItem.getYear() + "0" + saveItem.getMonth() + saveItem.getDay();
            }
        } else {
            if (saveItem.getDay() < 10) {
                actualDate = "" + saveItem.getYear() + saveItem.getMonth() + "0" + saveItem.getDay();
            } else {
                actualDate = "" + saveItem.getYear() + saveItem.getMonth() + saveItem.getDay();
            }
        }
        Attendance attendance = new Attendance(
                null,
                status,
                date,
                actualDate,
                clickedSubject.getId()
        );
        try {
            attendanceDao.insertOrReplace(attendance);

            Subjects updatedSubject = clickedSubject;
            if (status.equals(getResources().getString(R.string.status_attended))) {
                int totalClasses, attendedClasses, safeBunks;
                float currentPercentage, passPercentage;

                totalClasses = clickedSubject.getTotal();
                attendedClasses = clickedSubject.getAttend();

                totalClasses = totalClasses + 1;
                attendedClasses = attendedClasses + 1;

                currentPercentage = calculatePercentage(attendedClasses, totalClasses);
                passPercentage = clickedSubject.getPass_per();

                safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

                updatedSubject.setCur_per(currentPercentage);
                updatedSubject.setSafe_bunks(safeBunks);
                updatedSubject.setTotal(totalClasses);
                updatedSubject.setAttend(attendedClasses);

            } else if (status.equals(getResources().getString(R.string.status_bunked))) {

                int totalClasses, bunkedClasses, attendedClasses, safeBunks;
                float currentPercentage, passPercentage;
                totalClasses = clickedSubject.getTotal();
                bunkedClasses = clickedSubject.getBunk();
                attendedClasses = clickedSubject.getAttend();

                totalClasses = totalClasses + 1;
                bunkedClasses = bunkedClasses + 1;

                currentPercentage = calculatePercentage(attendedClasses, totalClasses);
                passPercentage = clickedSubject.getPass_per();

                safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

                updatedSubject.setCur_per(currentPercentage);
                updatedSubject.setSafe_bunks(safeBunks);
                updatedSubject.setTotal(totalClasses);
                updatedSubject.setBunk(bunkedClasses);
            }

            subjectsDao.delete(clickedSubject);
            subjectsDao.insert(updatedSubject);

            fragmentTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fragmentTransaction() {
        if (saveItem.isTabletUI()) {
            Fragment subjectListFragment = new ActivitySubjectList();
            fragmentTransaction.replace(R.id.subject_list, subjectListFragment);
            Bundle bundle = new Bundle();
            bundle.putParcelable("data", saveItem);
            subjectListFragment.setArguments(bundle);

            Fragment subjectDetailFragment = new ActivitySubjectDetail();
            fragmentTransaction.replace(R.id.subject_details, subjectDetailFragment);
            Bundle bundleDetail = new Bundle();
            bundleDetail.putParcelable("data", saveItem);
            subjectDetailFragment.setArguments(bundleDetail);

        } else {
            if (saveItem.isListFragment()) {
                Fragment subjectListFragment = new ActivitySubjectList();
                fragmentTransaction.replace(R.id.phone_layout, subjectListFragment);
                Bundle bundle = new Bundle();
                bundle.putParcelable("data", saveItem);
                subjectListFragment.setArguments(bundle);
            } else {
                if (saveItem.isAttendanceFragment()) {
                    Fragment attendanceFragment = new ActivityAttendance();
                    fragmentTransaction.replace(R.id.phone_layout, attendanceFragment);
                    Bundle bundleDetail = new Bundle();
                    bundleDetail.putParcelable("data", saveItem);
                    attendanceFragment.setArguments(bundleDetail);
                } else {
                    Fragment subjectDetailFragment = new ActivitySubjectDetail();
                    fragmentTransaction.replace(R.id.phone_layout, subjectDetailFragment);
                    Bundle bundleDetail = new Bundle();
                    bundleDetail.putParcelable("data", saveItem);
                    subjectDetailFragment.setArguments(bundleDetail);
                }
            }
        }

        fragmentTransaction.commit();

    }

    private int findSafeBunks(float currentPercentage, float passPercentage, int attend, int total) {
        float attendedClasses = attend, totalClasses = total;
        int safeBunks = 0;
        float percentage = currentPercentage;

        if (passPercentage < currentPercentage) {
            while (passPercentage < percentage) {
                percentage = (attendedClasses / (++totalClasses)) * 100;
                safeBunks = safeBunks + 1;
            }
            return (safeBunks - 1);
        } else {
            while (passPercentage > percentage) {
                percentage = ((++attendedClasses) / (++totalClasses)) * 100;
                safeBunks = safeBunks + 1;
            }
            return (-1 * safeBunks);
        }

    }

    private float calculatePercentage(int attendedClasses, int totalClasses) {
        float percentage = (float) attendedClasses / (float) totalClasses;
        return (percentage * 100);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        try {
            saveItem.setEditSubName(newSubjectName.getText().toString());
            saveItem.setEditPassPer(newPassPercentage.getText().toString());
            saveItem.setNewColor(saveItem.getNewColor());
            saveItem.setEditColorPicker(saveItem.isColorSelected());
        } catch (Exception e) {
            e.printStackTrace();
        }

        saveItem.setScreenRotated(true);


    }
}
