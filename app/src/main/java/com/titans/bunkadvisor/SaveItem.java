package com.titans.bunkadvisor;

import android.os.Parcel;
import android.os.Parcelable;

public class SaveItem implements Parcelable {

    private boolean tabletUI;
    private boolean listFragment;
    private boolean attendanceFragment;
    private boolean popupInflated;
    private boolean colorPopupInflated;
    private boolean datePickerInflated;
    private boolean deletePopup;
    private boolean editPopup;
    private boolean editColorPicker;
    private boolean colorSelected;
    private boolean allAttendance;
    private boolean attendedOnly;
    private boolean screenRotated;

    private int intSelectedColor;
    private int position;
    private int attendancePosition;
    private int day, month, year;
    private int newColor;
    private int scrollPosition;

    private long id;
    private long currentUserID;

    private String subName, passPer, monthString, userName;
    private String editSubName, editPassPer;

    public SaveItem() {
        tabletUI = true;
        listFragment = true;
        attendanceFragment = false;
        popupInflated = false;
        colorPopupInflated = false;
        datePickerInflated = false;
        deletePopup = false;
        editPopup = false;
        editColorPicker = false;
        colorSelected = false;
        allAttendance = true;
        attendedOnly = false;
        screenRotated = false;

        intSelectedColor = 0;
        position = 0;
        attendancePosition = 0;
        day = 0;
        month = 0;
        year = 0;
        newColor = 0;
        scrollPosition = 0;

        id = 0;
        currentUserID = 0;

        subName = null;
        passPer = null;
        monthString = null;
        userName = null;
        editSubName = null;
        editPassPer = null;
    }


    public SaveItem(Parcel parcel) {

        tabletUI = parcel.readInt() == 1;
        listFragment = parcel.readInt() == 1;
        attendanceFragment = parcel.readInt() == 1;
        popupInflated = parcel.readInt() == 1;
        colorPopupInflated = parcel.readInt() == 1;
        datePickerInflated = parcel.readInt() == 1;
        deletePopup = parcel.readInt() == 1;
        editPopup = parcel.readInt() == 1;
        editColorPicker = parcel.readInt() == 1;
        colorSelected = parcel.readInt() == 1;
        allAttendance = parcel.readInt() == 1;
        attendedOnly = parcel.readInt() == 1;
        screenRotated = parcel.readInt() == 1;

        intSelectedColor = parcel.readInt();
        position = parcel.readInt();
        attendancePosition = parcel.readInt();
        day = parcel.readInt();
        month = parcel.readInt();
        year = parcel.readInt();
        newColor = parcel.readInt();
        scrollPosition = parcel.readInt();

        id = parcel.readLong();
        currentUserID = parcel.readLong();

        subName = parcel.readString();
        passPer = parcel.readString();
        monthString = parcel.readString();
        userName = parcel.readString();
        editSubName = parcel.readString();
        editPassPer = parcel.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        if (tabletUI)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (listFragment)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (attendanceFragment)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (popupInflated)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (colorPopupInflated)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (datePickerInflated)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (deletePopup)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (editPopup)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (editColorPicker)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (colorSelected)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (allAttendance)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (attendedOnly)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        if (screenRotated)
            parcel.writeInt(1);
        else
            parcel.writeInt(0);

        parcel.writeInt(intSelectedColor);
        parcel.writeInt(position);
        parcel.writeInt(attendancePosition);
        parcel.writeInt(day);
        parcel.writeInt(month);
        parcel.writeInt(year);
        parcel.writeInt(newColor);
        parcel.writeInt(scrollPosition);

        parcel.writeLong(id);
        parcel.writeLong(currentUserID);

        parcel.writeString(subName);
        parcel.writeString(passPer);
        parcel.writeString(monthString);
        parcel.writeString(userName);
        parcel.writeString(editSubName);
        parcel.writeString(editPassPer);

    }

    public static final Parcelable.Creator<SaveItem> CREATOR
            = new Parcelable.Creator<SaveItem>() {
        public SaveItem createFromParcel(Parcel in) {
            return new SaveItem(in);
        }

        public SaveItem[] newArray(int size) {
            return new SaveItem[size];
        }
    };

    public boolean isTabletUI() {
        return tabletUI;
    }

    public void setTabletUI(boolean tabletUI) {
        this.tabletUI = tabletUI;
    }

    public boolean isListFragment() {
        return listFragment;
    }

    public void setListFragment(boolean listFragment) {
        this.listFragment = listFragment;
    }

    public boolean isAttendanceFragment() {
        return attendanceFragment;
    }

    public void setAttendanceFragment(boolean attendanceFragment) {
        this.attendanceFragment = attendanceFragment;
    }

    public boolean isPopupInflated() {
        return popupInflated;
    }

    public void setPopupInflated(boolean popupInflated) {
        this.popupInflated = popupInflated;
    }

    public boolean isColorPopupInflated() {
        return colorPopupInflated;
    }

    public void setColorPopupInflated(boolean colorPopupInflated) {
        this.colorPopupInflated = colorPopupInflated;
    }

    public boolean isDatePickerInflated() {
        return datePickerInflated;
    }

    public void setDatePickerInflated(boolean datePickerInflated) {
        this.datePickerInflated = datePickerInflated;
    }

    public boolean isDeletePopup() {
        return deletePopup;
    }

    public void setDeletePopup(boolean deletePopup) {
        this.deletePopup = deletePopup;
    }

    public boolean isEditPopup() {
        return editPopup;
    }

    public void setEditPopup(boolean editPopup) {
        this.editPopup = editPopup;
    }

    public boolean isEditColorPicker() {
        return editColorPicker;
    }

    public void setEditColorPicker(boolean editColorPicker) {
        this.editColorPicker = editColorPicker;
    }

    public boolean isColorSelected() {
        return colorSelected;
    }

    public void setColorSelected(boolean colorSelected) {
        this.colorSelected = colorSelected;
    }

    public boolean isAllAttendance() {
        return allAttendance;
    }

    public void setAllAttendance(boolean allAttendance) {
        this.allAttendance = allAttendance;
    }

    public boolean isAttendedOnly() {
        return attendedOnly;
    }

    public void setAttendedOnly(boolean attendedOnly) {
        this.attendedOnly = attendedOnly;
    }

    public boolean isScreenRotated() {
        return screenRotated;
    }

    public void setScreenRotated(boolean screenRotated) {
        this.screenRotated = screenRotated;
    }

    public int getIntSelectedColor() {
        return intSelectedColor;
    }

    public void setIntSelectedColor(int intSelectedColor) {
        this.intSelectedColor = intSelectedColor;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getAttendancePosition() {
        return attendancePosition;
    }

    public void setAttendancePosition(int attendancePosition) {
        this.attendancePosition = attendancePosition;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNewColor() {
        return newColor;
    }

    public void setNewColor(int newColor) {
        this.newColor = newColor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCurrentUserID() {
        return currentUserID;
    }

    public void setCurrentUserID(long currentUserID) {
        this.currentUserID = currentUserID;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getPassPer() {
        return passPer;
    }

    public void setPassPer(String passPer) {
        this.passPer = passPer;
    }

    public String getMonthString() {
        return monthString;
    }

    public void setMonthString(String monthString) {
        this.monthString = monthString;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEditSubName() {
        return editSubName;
    }

    public void setEditSubName(String editSubName) {
        this.editSubName = editSubName;
    }

    public String getEditPassPer() {
        return editPassPer;
    }

    public void setEditPassPer(String editPassPer) {
        this.editPassPer = editPassPer;
    }
}
