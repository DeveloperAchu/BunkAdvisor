package com.titans.bunkadvisor;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.titans.bunkadvisor.model.Attendance;
import com.titans.bunkadvisor.model.AttendanceDao;
import com.titans.bunkadvisor.model.DaoMaster;
import com.titans.bunkadvisor.model.DaoSession;
import com.titans.bunkadvisor.model.Subjects;
import com.titans.bunkadvisor.model.SubjectsDao;

import java.util.List;

public class ActivitySubjectList extends Fragment implements AdapterSubjects.ClickListener, AdapterSubjects.AttendedClickListener, AdapterSubjects.BunkedClickListener {

    RecyclerView recycleSubjectsCard;
    DaoMaster daoMaster;
    DaoMaster.DevOpenHelper helper;
    SQLiteDatabase db;
    DaoSession daoSession;
    SubjectsDao subjectsDao;
    AttendanceDao attendanceDao;
    List<Subjects> subjectsList;
    SaveItem saveItem;
    boolean tabletUI;
    boolean listFragment;
    RecyclerViewPositionHelper mRecyclerViewHelper;
    int firstVisibleItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subject_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            saveItem = bundle.getParcelable("data");
        }

        tabletUI = saveItem.isTabletUI();
        try {
            if (!tabletUI)
                ((MainActivity) getActivity()).fab.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        saveItem.setListFragment(true);
        saveItem.setAttendanceFragment(false);

        Context context = getContext();

        recycleSubjectsCard = (RecyclerView) getActivity().findViewById(R.id.card_list);

        listFragment = saveItem.isListFragment();
        saveItem.setId(0);

        helper = new DaoMaster.DevOpenHelper(context, "bunk-db", null);
        db = helper.getReadableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        subjectsDao = daoSession.getSubjectsDao();
        attendanceDao = daoSession.getAttendanceDao();
        try {
            subjectsList = subjectsDao.queryBuilder()
                    .where(SubjectsDao.Properties.User_id.eq(saveItem.getCurrentUserID()))
                    .orderDesc(SubjectsDao.Properties.Id)
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        AdapterSubjects adapter = new AdapterSubjects(subjectsList, context);
        recycleSubjectsCard.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.setClickListener(this);
        adapter.setAttendedClickListener(this);
        adapter.setBunkedClickListener(this);
        recycleSubjectsCard.setAdapter(adapter);

        recycleSubjectsCard.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);
                firstVisibleItem = mRecyclerViewHelper.findFirstVisibleItemPosition();
                saveItem.setPosition(firstVisibleItem);
            }

        });

        try {
            recycleSubjectsCard.getLayoutManager().scrollToPosition(saveItem.getPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void itemClicked(View v, int position) {

        saveItem.setPosition(firstVisibleItem);
        saveItem.setId(subjectsList.get(position).getId());

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        if (tabletUI) {
            Fragment subjectDetailFragment = new ActivitySubjectDetail();
            fragmentTransaction.replace(R.id.subject_details, subjectDetailFragment);
            Bundle bundleDetail = new Bundle();
            bundleDetail.putParcelable("data", saveItem);
            subjectDetailFragment.setArguments(bundleDetail);
        } else {
            Fragment subjectDetailFragment = new ActivitySubjectDetail();
            fragmentTransaction.replace(R.id.phone_layout, subjectDetailFragment);
            Bundle bundleDetail = new Bundle();
            bundleDetail.putParcelable("data", saveItem);
            subjectDetailFragment.setArguments(bundleDetail);
        }

        fragmentTransaction.commit();

    }

    @Override
    public void attendedClicked(int position) {
        attendanceButtonClicked(position, getResources().getString(R.string.status_attended));
    }

    @Override
    public void bunkedClicked(int position) {
        attendanceButtonClicked(position, getResources().getString(R.string.status_bunked));
    }

    private void attendanceButtonClicked(int position, String status) {
        String date = ((MainActivity) getActivity()).datePickerButton.getText().toString();
        Subjects clickedSubject = subjectsList.get(position);
        String actualDate;
        if (saveItem.getMonth() < 10) {
            if (saveItem.getDay() < 10) {
                actualDate = "" + saveItem.getYear() + "0" + saveItem.getMonth() + "0" + saveItem.getDay();
            } else {
                actualDate = "" + saveItem.getYear() + "0" + saveItem.getMonth() + saveItem.getDay();
            }
        } else {
            if (saveItem.getDay() < 10) {
                actualDate = "" + saveItem.getYear() + saveItem.getMonth() + "0" + saveItem.getDay();
            } else {
                actualDate = "" + saveItem.getYear() + saveItem.getMonth() + saveItem.getDay();
            }
        }
        Attendance attendance = new Attendance(
                null,
                status,
                date,
                actualDate,
                clickedSubject.getId()
        );
        try {
            attendanceDao.insertOrReplace(attendance);

            Subjects updatedSubject = clickedSubject;
            if (status.equals(getResources().getString(R.string.status_attended))) {
                int totalClasses, attendedClasses, safeBunks;
                float currentPercentage, passPercentage;

                totalClasses = clickedSubject.getTotal();
                attendedClasses = clickedSubject.getAttend();

                totalClasses = totalClasses + 1;
                attendedClasses = attendedClasses + 1;

                currentPercentage = calculatePercentage(attendedClasses, totalClasses);
                passPercentage = clickedSubject.getPass_per();

                safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

                updatedSubject.setCur_per(currentPercentage);
                updatedSubject.setSafe_bunks(safeBunks);
                updatedSubject.setTotal(totalClasses);
                updatedSubject.setAttend(attendedClasses);

            } else if (status.equals(getResources().getString(R.string.status_bunked))) {

                int totalClasses, bunkedClasses, attendedClasses, safeBunks;
                float currentPercentage, passPercentage;
                totalClasses = clickedSubject.getTotal();
                bunkedClasses = clickedSubject.getBunk();
                attendedClasses = clickedSubject.getAttend();

                totalClasses = totalClasses + 1;
                bunkedClasses = bunkedClasses + 1;

                currentPercentage = calculatePercentage(attendedClasses, totalClasses);
                passPercentage = clickedSubject.getPass_per();

                safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

                updatedSubject.setCur_per(currentPercentage);
                updatedSubject.setSafe_bunks(safeBunks);
                updatedSubject.setTotal(totalClasses);
                updatedSubject.setBunk(bunkedClasses);
            }

            subjectsDao.delete(clickedSubject);
            subjectsDao.insert(updatedSubject);
            saveItem.setPosition(firstVisibleItem);
            fragmentTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fragmentTransaction() {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        if (tabletUI) {
            Fragment subjectListFragment = new ActivitySubjectList();
            fragmentTransaction.replace(R.id.subject_list, subjectListFragment);
            Bundle bundleList = new Bundle();
            bundleList.putParcelable("data", saveItem);
            subjectListFragment.setArguments(bundleList);

            Fragment subjectDetailFragment = new ActivitySubjectDetail();
            fragmentTransaction.replace(R.id.subject_details, subjectDetailFragment);
            Bundle bundleDetail = new Bundle();
            bundleDetail.putParcelable("data", saveItem);
            subjectDetailFragment.setArguments(bundleDetail);
        } else {
            if (listFragment) {
                Fragment subjectListFragment = new ActivitySubjectList();
                fragmentTransaction.replace(R.id.phone_layout, subjectListFragment);
                Bundle bundle = new Bundle();
                bundle.putParcelable("data", saveItem);
                subjectListFragment.setArguments(bundle);
            } else {
                Fragment subjectDetailFragment = new ActivitySubjectDetail();
                fragmentTransaction.replace(R.id.phone_layout, subjectDetailFragment);
                Bundle bundleDetail = new Bundle();
                bundleDetail.putParcelable("data", saveItem);
                subjectDetailFragment.setArguments(bundleDetail);
            }
        }

        fragmentTransaction.commit();

    }

    private int findSafeBunks(float currentPercentage, float passPercentage, int attend, int total) {
        float attendedClasses = attend, totalClasses = total;
        int safeBunks = 0;
        float percentage = currentPercentage;

        if (passPercentage < currentPercentage) {
            while (passPercentage < percentage) {
                percentage = (attendedClasses / (++totalClasses)) * 100;
                safeBunks = safeBunks + 1;
            }
            return (safeBunks - 1);
        } else {
            while (passPercentage > percentage) {
                percentage = ((++attendedClasses) / (++totalClasses)) * 100;
                safeBunks = safeBunks + 1;
            }
            return (-1 * safeBunks);
        }

    }

    private float calculatePercentage(int attendedClasses, int totalClasses) {
        if (totalClasses == 0)
            return 0.0f;
        float percentage = (float) attendedClasses / (float) totalClasses;
        return (percentage * 100);
    }

}
