package com.titans.bunkadvisor;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import com.titans.bunkadvisor.model.Attendance;
import com.titans.bunkadvisor.model.AttendanceDao;
import com.titans.bunkadvisor.model.DaoMaster;
import com.titans.bunkadvisor.model.DaoSession;
import com.titans.bunkadvisor.model.Subjects;
import com.titans.bunkadvisor.model.SubjectsDao;

import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

public class ActivityAttendance extends Fragment implements AdapterAttendance.ButtonClickListener {

    Context context;
    RecyclerView recyclerAttendanceList;
    LayoutInflater layoutInflater;
    View view;
    SaveItem saveItem;
    boolean tabletUI;
    String currentStatus;

    DaoMaster daoMaster;
    DaoMaster.DevOpenHelper helper;
    SQLiteDatabase db;
    DaoSession daoSession;
    SubjectsDao subjectsDao;
    AttendanceDao attendanceDao;
    List<Attendance> attendanceList;
    Subjects subject;
    Attendance currentAttendance, deletedAttendance;

    ImageButton filterButton;
    LinearLayout linearLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.layoutInflater = inflater;
        this.view = container;
        return inflater.inflate(R.layout.attendance_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            saveItem = bundle.getParcelable("data");
        }

        try {
            tabletUI = saveItem.isTabletUI();
            if (!tabletUI)
                ((MainActivity) getActivity()).fab.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }

        saveItem.setListFragment(false);
        saveItem.setAttendanceFragment(true);

        context = getContext();

        recyclerAttendanceList = (RecyclerView) getActivity().findViewById(R.id.attendance_list);
        linearLayout = (LinearLayout) view.findViewById(R.id.attendance_coordinate_layout);

        helper = new DaoMaster.DevOpenHelper(context, "bunk-db", null);
        db = helper.getReadableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        subjectsDao = daoSession.getSubjectsDao();
        attendanceDao = daoSession.getAttendanceDao();

        try {
            subject = subjectsDao.load(saveItem.getId());

            if (saveItem.isAllAttendance()) {
                QueryBuilder qb = attendanceDao.queryBuilder();
                qb.where(AttendanceDao.Properties.Subjects_id.eq(subject.getId()));
                qb.orderDesc(AttendanceDao.Properties.ActualDate);
                attendanceList = qb.list();
            } else {
                if (saveItem.isAttendedOnly()) {
                    QueryBuilder qb = attendanceDao.queryBuilder();
                    qb.where(AttendanceDao.Properties.Subjects_id.eq(subject.getId()));
                    qb.where(AttendanceDao.Properties.Status.eq(getResources().getString(R.string.status_attended)));
                    qb.orderDesc(AttendanceDao.Properties.ActualDate);
                    attendanceList = qb.list();
                } else {
                    QueryBuilder qb = attendanceDao.queryBuilder();
                    qb.where(AttendanceDao.Properties.Subjects_id.eq(subject.getId()));
                    qb.where(AttendanceDao.Properties.Status.eq(getResources().getString(R.string.status_bunked)));
                    qb.orderDesc(AttendanceDao.Properties.ActualDate);
                    attendanceList = qb.list();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        AdapterAttendance adapter = new AdapterAttendance(attendanceList, context);
        recyclerAttendanceList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.setClickListener(this);
        recyclerAttendanceList.setAdapter(adapter);

        filterButton = (ImageButton) getActivity().findViewById(R.id.filter_button);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getContext(), view);
                popupMenu.inflate(R.menu.filter_options);
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals(getResources().getString(R.string.show_all))) {
                            saveItem.setAllAttendance(true);
                            saveItem.setAttendedOnly(false);
                        } else if (item.getTitle().equals(getResources().getString(R.string.attended_only))) {
                            saveItem.setAllAttendance(false);
                            saveItem.setAttendedOnly(true);
                        } else if (item.getTitle().equals(getResources().getString(R.string.bunked_only))) {
                            saveItem.setAllAttendance(false);
                            saveItem.setAttendedOnly(false);

                        }
                        fragmentTransaction();
                        return true;
                    }
                });

            }
        });

    }

    @Override
    public void buttonClicked(View v, int position) {
        currentStatus = attendanceList.get(position).getStatus();
        currentAttendance = attendanceList.get(position);
        saveItem.setAttendancePosition(position);
        PopupMenu popupMenu = new PopupMenu(getContext(), v);

        if (currentStatus.equals(getResources().getString(R.string.status_attended))) {
            popupMenu.inflate(R.menu.mark_as_bunked_popup);
        } else if (currentStatus.equals(getResources().getString(R.string.status_bunked))) {
            popupMenu.inflate(R.menu.mark_as_attended_popup);
        }

        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals(getResources().getString(R.string.mark_as_attended))) {
                    changeTo(1);
                } else if (item.getTitle().equals(getResources().getString(R.string.mark_as_bunked))) {
                    changeTo(0);
                } else if (item.getTitle().equals(getResources().getString(R.string.delete_attendance))) {
                    deleteClicked();
                }
                fragmentTransaction();
                return true;
            }
        });
    }

    private void deleteClicked() {
        int attendedClasses, bunkedClasses, totalClasses;
        Subjects updatedSubject = subject;

        currentAttendance = attendanceList.get(saveItem.getAttendancePosition());
        deletedAttendance = currentAttendance;
        currentStatus = attendanceList.get(saveItem.getAttendancePosition()).getStatus();

        if (currentStatus.equals(getResources().getString(R.string.status_attended))) {
            attendedClasses = subject.getAttend();
            totalClasses = subject.getTotal();

            attendedClasses = attendedClasses - 1;
            totalClasses = totalClasses - 1;

            float currentPercentage = calculatePercentage(attendedClasses, totalClasses);
            float passPercentage = subject.getPass_per();

            int safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

            updatedSubject.setCur_per(currentPercentage);
            updatedSubject.setSafe_bunks(safeBunks);
            updatedSubject.setTotal(totalClasses);
            updatedSubject.setAttend(attendedClasses);

        } else {
            attendedClasses = subject.getAttend();
            bunkedClasses = subject.getBunk();
            totalClasses = subject.getTotal();

            bunkedClasses = bunkedClasses - 1;
            totalClasses = totalClasses - 1;

            float currentPercentage = calculatePercentage(attendedClasses, totalClasses);
            float passPercentage = subject.getPass_per();

            int safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

            updatedSubject.setCur_per(currentPercentage);
            updatedSubject.setSafe_bunks(safeBunks);
            updatedSubject.setTotal(totalClasses);
            updatedSubject.setBunk(bunkedClasses);
        }

        try {
            long ID = currentAttendance.getId();
            attendanceDao.deleteByKey(ID);
            subjectsDao.delete(subject);
            subjectsDao.insert(updatedSubject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showSnackBar("Attendance deleted");
    }

    private void showSnackBar(String msg) {
        Snackbar.make(linearLayout, msg, Snackbar.LENGTH_SHORT)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        insertAttendance();
                        fragmentTransaction();

                    }
                })
                .setActionTextColor(ContextCompat.getColor(context, R.color.yellowColor))
                .show();
    }

    private void insertAttendance() {
        Subjects updatedSubject = subject;
        if (deletedAttendance.getStatus().equals(context.getResources().getString(R.string.status_attended))) {
            int totalClasses, attendedClasses, safeBunks;
            float currentPercentage, passPercentage;

            totalClasses = subject.getTotal();
            attendedClasses = subject.getAttend();

            totalClasses = totalClasses + 1;
            attendedClasses = attendedClasses + 1;

            currentPercentage = calculatePercentage(attendedClasses, totalClasses);
            passPercentage = subject.getPass_per();

            safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

            updatedSubject.setCur_per(currentPercentage);
            updatedSubject.setSafe_bunks(safeBunks);
            updatedSubject.setTotal(totalClasses);
            updatedSubject.setAttend(attendedClasses);

        } else if (deletedAttendance.getStatus().equals(context.getResources().getString(R.string.status_bunked))) {

            int totalClasses, bunkedClasses, attendedClasses, safeBunks;
            float currentPercentage, passPercentage;
            totalClasses = subject.getTotal();
            bunkedClasses = subject.getBunk();
            attendedClasses = subject.getAttend();

            totalClasses = totalClasses + 1;
            bunkedClasses = bunkedClasses + 1;

            currentPercentage = calculatePercentage(attendedClasses, totalClasses);
            passPercentage = subject.getPass_per();

            safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

            updatedSubject.setCur_per(currentPercentage);
            updatedSubject.setSafe_bunks(safeBunks);
            updatedSubject.setTotal(totalClasses);
            updatedSubject.setBunk(bunkedClasses);
        }

        try {
            subjectsDao.delete(subject);
            subjectsDao.insert(updatedSubject);
            attendanceDao.insert(deletedAttendance);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void fragmentTransaction() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        if (saveItem.isTabletUI()) {
            Fragment subjectListFragment = new ActivitySubjectList();
            fragmentTransaction.replace(R.id.subject_list, subjectListFragment);
            Bundle bundle = new Bundle();
            bundle.putParcelable("data", saveItem);
            subjectListFragment.setArguments(bundle);

            Fragment subjectDetailFragment = new ActivitySubjectDetail();
            fragmentTransaction.replace(R.id.subject_details, subjectDetailFragment);
            Bundle bundleDetail = new Bundle();
            bundleDetail.putParcelable("data", saveItem);
            subjectDetailFragment.setArguments(bundleDetail);

        } else {
            if (saveItem.isListFragment()) {
                Fragment subjectListFragment = new ActivitySubjectList();
                fragmentTransaction.replace(R.id.phone_layout, subjectListFragment);
                Bundle bundle = new Bundle();
                bundle.putParcelable("data", saveItem);
                subjectListFragment.setArguments(bundle);
            } else {
                if (saveItem.isAttendanceFragment()) {
                    Fragment attendanceFragment = new ActivityAttendance();
                    fragmentTransaction.replace(R.id.phone_layout, attendanceFragment);
                    Bundle bundleDetail = new Bundle();
                    bundleDetail.putParcelable("data", saveItem);
                    attendanceFragment.setArguments(bundleDetail);
                } else {
                    Fragment subjectDetailFragment = new ActivitySubjectDetail();
                    fragmentTransaction.replace(R.id.phone_layout, subjectDetailFragment);
                    Bundle bundleDetail = new Bundle();
                    bundleDetail.putParcelable("data", saveItem);
                    subjectDetailFragment.setArguments(bundleDetail);
                }
            }
        }

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private int findSafeBunks(float currentPercentage, float passPercentage, int attend, int total) {
        float attendedClasses = attend, totalClasses = total;
        int safeBunks = 0;
        float percentage = currentPercentage;

        if (currentPercentage == 0.0f)
            return 0;

        if (passPercentage < currentPercentage) {
            while (passPercentage < percentage) {
                percentage = (attendedClasses / (++totalClasses)) * 100;
                safeBunks = safeBunks + 1;
            }
            return (safeBunks - 1);
        } else {
            while (passPercentage > percentage) {
                percentage = ((++attendedClasses) / (++totalClasses)) * 100;
                safeBunks = safeBunks + 1;
            }
            return (-1 * safeBunks);
        }
    }

    private float calculatePercentage(int attendedClasses, int totalClasses) {
        if (totalClasses == 0)
            return 0.0f;
        float percentage = (float) attendedClasses / (float) totalClasses;
        return (percentage * 100);
    }

    private void changeTo(int to) {
        int attendedClasses, bunkedClasses, totalClasses, safeBunks;
        float currentPercentage, passPercentage;
        Subjects updatedSubject = subject;
        Attendance updatedAttendance = currentAttendance;

        switch (to) {
            case 0:
                attendedClasses = subject.getAttend();
                bunkedClasses = subject.getBunk();
                totalClasses = subject.getTotal();

                bunkedClasses = bunkedClasses + 1;
                attendedClasses = attendedClasses - 1;

                currentPercentage = calculatePercentage(attendedClasses, totalClasses);
                passPercentage = subject.getPass_per();

                safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

                updatedSubject.setCur_per(currentPercentage);
                updatedSubject.setSafe_bunks(safeBunks);
                updatedSubject.setAttend(attendedClasses);
                updatedSubject.setBunk(bunkedClasses);

                updatedAttendance.setStatus(getResources().getString(R.string.status_bunked));
                break;

            case 1:
                attendedClasses = subject.getAttend();
                bunkedClasses = subject.getBunk();
                totalClasses = subject.getTotal();

                attendedClasses = attendedClasses + 1;
                bunkedClasses = bunkedClasses - 1;

                currentPercentage = calculatePercentage(attendedClasses, totalClasses);
                passPercentage = subject.getPass_per();

                safeBunks = findSafeBunks(currentPercentage, passPercentage, attendedClasses, totalClasses);

                updatedSubject.setCur_per(currentPercentage);
                updatedSubject.setSafe_bunks(safeBunks);
                updatedSubject.setAttend(attendedClasses);
                updatedSubject.setBunk(bunkedClasses);

                updatedAttendance.setStatus(getResources().getString(R.string.status_attended));
                break;
        }
        subjectsDao.delete(subject);
        subjectsDao.insert(updatedSubject);

        attendanceDao.delete(currentAttendance);
        attendanceDao.insert(updatedAttendance);

        fragmentTransaction();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
