package com.titans.bunkadvisor;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;
import com.titans.bunkadvisor.model.DaoMaster;
import com.titans.bunkadvisor.model.DaoSession;
import com.titans.bunkadvisor.model.Subjects;
import com.titans.bunkadvisor.model.SubjectsDao;
import com.titans.bunkadvisor.model.Users;
import com.titans.bunkadvisor.model.UsersDao;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity {

    private int SELECT_FILE = 1;

    final Context context = this;
    SaveItem saveItem;
    private long newSubjectID, newUserID;
    Uri uri;

    FloatingActionButton fab;
    View promptsView;
    LayoutInflater layoutInflater;
    AlertDialog.Builder alertDialogBuilder;
    ColorPicker picker;
    AlertDialog alertDialog;
    EditText subjectName, passPercentage, userNameEditText;
    Button datePickerButton;
    CoordinatorLayout coordinatorLayout;

    private DrawerLayout drawerLayout;
    CircleImageView profilePicture;

    DaoMaster daoMaster;
    DaoMaster.DevOpenHelper helper;
    SQLiteDatabase db;
    DaoSession daoSession;
    SubjectsDao subjectsDao;
    UsersDao usersDao;

    SharedPreferences settings;
    final String SHARED_DATA = "sharedData";
    final String CURRENT_USER_NAME = "currentUserName";
    final String CURRENT_USER_ID = "currentUserID";
    final String FIRST_LAUNCH = "firstLaunch";
    final String PROFILE_PIC_ADDED = "profilePictureAdded";
    final String IMAGE_SELECTED = "imageSelected";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        saveItem = new SaveItem();

        saveItem.setTabletUI(checkDevice());

        final Calendar c = Calendar.getInstance();
        saveItem.setYear(c.get(Calendar.YEAR));
        saveItem.setMonth(c.get(Calendar.MONTH));
        saveItem.setMonth(saveItem.getMonth() + 1);
        saveItem.setMonthString(findMonth(saveItem.getMonth()));
        saveItem.setDay(c.get(Calendar.DAY_OF_MONTH));

        uri = Uri.parse("android.resource://com.titans.bunkadvisor/drawable/new_user");

    }

    private String findMonth(int month) {
        String monthString = " ";
        switch (month) {
            case 1:
                monthString = "January";
                break;

            case 2:
                monthString = "February";
                break;

            case 3:
                monthString = "March";
                break;

            case 4:
                monthString = "April";
                break;

            case 5:
                monthString = "May";
                break;

            case 6:
                monthString = "June";
                break;

            case 7:
                monthString = "July";
                break;

            case 8:
                monthString = "August";
                break;

            case 9:
                monthString = "September";
                break;

            case 10:
                monthString = "October";
                break;

            case 11:
                monthString = "November";
                break;

            case 12:
                monthString = "December";
                break;

        }
        return monthString;
    }

    private boolean checkDevice() {

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.tablet_layout);
        return (frameLayout != null);

    }

    @Override
    protected void onPause() {
        super.onPause();

        Release();

    }

    @Override
    public void onBackPressed() {
        Release();

        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        if (!saveItem.isListFragment() && !saveItem.isTabletUI()) {
            if (saveItem.isAttendanceFragment()) {
                Fragment subjectDetailFragment = new ActivitySubjectDetail();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.phone_layout, subjectDetailFragment);

                Bundle bundleList = new Bundle();
                bundleList.putParcelable("data", saveItem);
                subjectDetailFragment.setArguments(bundleList);

                fragmentTransaction.commit();
                return;
            } else {
                Fragment subjectListFragment = new ActivitySubjectList();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.phone_layout, subjectListFragment);

                Bundle bundleList = new Bundle();
                bundleList.putParcelable("data", saveItem);
                subjectListFragment.setArguments(bundleList);

                fragmentTransaction.commit();
                return;
            }
        } else if (saveItem.isTabletUI()) {
            if (saveItem.isAttendanceFragment()) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                Fragment subjectListFragment = new ActivitySubjectList();
                fragmentTransaction.replace(R.id.subject_list, subjectListFragment);
                Bundle bundleAttendance = new Bundle();
                bundleAttendance.putParcelable("data", saveItem);
                subjectListFragment.setArguments(bundleAttendance);

                Fragment subjectDetailFragment = new ActivitySubjectDetail();
                fragmentTransaction.replace(R.id.subject_details, subjectDetailFragment);
                Bundle bundleDetail = new Bundle();
                bundleDetail.putParcelable("data", saveItem);
                subjectDetailFragment.setArguments(bundleDetail);

                fragmentTransaction.commit();
                return;
            }
        }
        super.onBackPressed();
    }

    private void Release() {
        helper = null;
        db = null;
        daoMaster = null;
        daoSession = null;
        subjectsDao = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinate_layout);

        helper = new DaoMaster.DevOpenHelper(this, "bunk-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        subjectsDao = daoSession.getSubjectsDao();
        usersDao = daoSession.getUsersDao();

        datePickerButton = (Button) findViewById(R.id.date_picker_button);
        if (datePickerButton != null) {
            datePickerButton.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
        }
        fab = (FloatingActionButton) findViewById(R.id.fab);

        settings = getSharedPreferences(SHARED_DATA, 0);

        if (settings.getBoolean(FIRST_LAUNCH, true)) {
            createNewUser("Create new user", 0);
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.main_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                profilePicture = (CircleImageView) drawerView.findViewById(R.id.profile_image_in_side_bar);

                boolean profilePicAdded = settings.getBoolean(PROFILE_PIC_ADDED, true);

                if (!profilePicAdded) {
                    profilePicture.setImageURI(uri);
                } else {
                    String imagePath = usersDao.load(settings.getLong(CURRENT_USER_ID, 0)).getImage_uri();
                    if (Uri.parse(imagePath).equals(null))
                        profilePicture.setImageURI(uri);
                    else
                        profilePicture.setImageURI(Uri.parse(imagePath));
                }

                profilePicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        layoutInflater = LayoutInflater.from(context);
                        promptsView = layoutInflater.inflate(R.layout.select_option, null);

                        alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setView(promptsView)
                                .setCancelable(true);

                        try {
                            alertDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                        TextView removePicture = (TextView) promptsView.findViewById(R.id.remove_pic);
                        removePicture.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(context, "Image removed", Toast.LENGTH_SHORT).show();
                                profilePicture.setImageURI(uri);

                                long previousUserId = settings.getLong(CURRENT_USER_ID, 0);
                                Users currentUser = usersDao.load(previousUserId);
                                currentUser.setImage_uri(null);
                                usersDao.delete(currentUser);
                                usersDao.insert(currentUser);

                                SharedPreferences.Editor newSettings = settings.edit();
                                newSettings.putBoolean(PROFILE_PIC_ADDED, false);
                                newSettings.apply();
                                try {
                                    alertDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        TextView openGallery = (TextView) promptsView.findViewById(R.id.open_gallery);
                        openGallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(galleryIntent, SELECT_FILE);
                            }
                        });

                    }
                });

                TextView profileName = (TextView) drawerView.findViewById(R.id.user_name_in_side_bar);
                profileName.setText(settings.getString(CURRENT_USER_NAME, " "));

                profileName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createNewUser("Edit User Name", 2);
                    }
                });

                String[] sideListItems = getResources().getStringArray(R.array.side_list_items);

                ListView listView = (ListView) drawerView.findViewById(R.id.drawer_list);
                AdapterSideBar adapter = new AdapterSideBar(context, sideListItems);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i) {
                            case 0:
                                createNewUser("Create new user", 1);
                                break;
                            case 1:
                                switchUser();
                                break;
                            case 2:
                                deleteUser();
                                break;
                            case 3:
                                settingsMethod();
                                break;
                            default:
                                Toast.makeText(context, "Current Version is " + getResources().getString(R.string.version), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });

            }
        };
        drawerLayout.setDrawerListener(drawerToggle);

        saveItem.setCurrentUserID(settings.getLong(CURRENT_USER_ID, 0));

        if (!saveItem.isDatePickerInflated()) {
            String str = saveItem.getMonthString() + " " + saveItem.getDay() + ", " + saveItem.getYear();
            datePickerButton.setText(str);
        }

        datePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveItem.setMonth(saveItem.getMonth() - 1);
                datePicker();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveItem.setIntSelectedColor(0);
                saveItem.setSubName(null);
                saveItem.setPassPer(null);
                fabClicked();
            }
        });

        fragmentTransaction();

        if (saveItem.isListFragment()) {
            fab.show();
        } else {
            fab.hide();
        }

        if (saveItem.isPopupInflated())
            fabClicked();

        if (saveItem.isColorPopupInflated())
            colorPicker();

        if (saveItem.isDatePickerInflated()) {
            int currentMonth = saveItem.getMonth();
            currentMonth = currentMonth + 1;
            String str = findMonth(currentMonth) + " " + saveItem.getDay() + ", " + saveItem.getYear();
            datePickerButton.setText(str);
            datePicker();
        }

    }

    private void deleteUser() {
    }

    private void settingsMethod() {
    }

    private void switchUser() {


    }

    private void createNewUser(final String title, final int numberOfUsers) {

        layoutInflater = LayoutInflater.from(context);
        promptsView = layoutInflater.inflate(R.layout.create_user, null);

        userNameEditText = (EditText) promptsView.findViewById(R.id.user_name);
        userNameEditText.setText(saveItem.getUserName());

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                saveItem.setUserName(userNameEditText.getText().toString());

                                if (saveItem.getUserName() == null || saveItem.getUserName().equals("")) {
                                    Toast.makeText(context, "Enter a valid name", Toast.LENGTH_SHORT).show();
                                    layoutInflater = null;
                                    promptsView = null;
                                    createNewUser(title, numberOfUsers);
                                } else {

                                    try {

                                        if (numberOfUsers == 0) {

                                            Users user = new Users(
                                                    null,
                                                    saveItem.getUserName(),
                                                    "null"
                                            );

                                            newUserID = usersDao.insertOrReplace(user);
                                            saveItem.setCurrentUserID(newUserID);

                                            SharedPreferences.Editor newSettings = settings.edit();
                                            newSettings.putString(CURRENT_USER_NAME, saveItem.getUserName());
                                            newSettings.putLong(CURRENT_USER_ID, newUserID);
                                            newSettings.putBoolean(FIRST_LAUNCH, false);
                                            newSettings.putBoolean(PROFILE_PIC_ADDED, false);
                                            newSettings.putBoolean(IMAGE_SELECTED, false);
                                            newSettings.apply();

                                        } else if (numberOfUsers == 1) {

                                            Users user = new Users(
                                                    null,
                                                    saveItem.getUserName(),
                                                    "null"
                                            );

                                            try {
                                                newUserID = usersDao.insertOrReplace(user);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } else if (numberOfUsers == 2) {

                                            long previousUserId = settings.getLong(CURRENT_USER_ID, 0);
                                            Users currentUser = usersDao.load(previousUserId);
                                            currentUser.setUser_name(saveItem.getUserName());
                                            usersDao.delete(currentUser);
                                            usersDao.insert(currentUser);

                                            SharedPreferences.Editor newSettings = settings.edit();
                                            newSettings.putString(CURRENT_USER_NAME, saveItem.getUserName());
                                            newSettings.apply();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    layoutInflater = null;
                                    promptsView = null;
                                }

                                drawerLayout.closeDrawers();

                            }
                        });
        if (numberOfUsers == 0) {
            alertDialogBuilder.setNegativeButton("EXIT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            layoutInflater = null;
                            promptsView = null;
                            finish();
                        }
                    });
        } else if (numberOfUsers == 1) {
            userNameEditText.setText("");
            alertDialogBuilder.setNegativeButton("CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            layoutInflater = null;
                            promptsView = null;
                            try {
                                alertDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } else if (numberOfUsers == 2) {
            userNameEditText.setText(settings.getString(CURRENT_USER_NAME, ""));
            alertDialogBuilder.setNegativeButton("CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            layoutInflater = null;
                            promptsView = null;
                            try {
                                alertDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }

        try {
            alertDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(ContextCompat.getColor(context, R.color.greenColoredButton));

        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(ContextCompat.getColor(context, R.color.redColoredButton));

    }

    private void fragmentTransaction() {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (saveItem.isTabletUI()) {
            Fragment subjectListFragment = new ActivitySubjectList();
            fragmentTransaction.replace(R.id.subject_list, subjectListFragment);
            Bundle bundleList = new Bundle();
            bundleList.putParcelable("data", saveItem);
            subjectListFragment.setArguments(bundleList);

            Fragment subjectDetailFragment = new ActivitySubjectDetail();
            fragmentTransaction.replace(R.id.subject_details, subjectDetailFragment);
            Bundle bundleDetail = new Bundle();
            bundleDetail.putParcelable("data", saveItem);
            subjectDetailFragment.setArguments(bundleDetail);
        } else {
            if (saveItem.isListFragment()) {
                Fragment subjectListFragment = new ActivitySubjectList();
                fragmentTransaction.replace(R.id.phone_layout, subjectListFragment);
                Bundle bundle = new Bundle();
                bundle.putParcelable("data", saveItem);
                subjectListFragment.setArguments(bundle);
            } else {
                if (saveItem.isAttendanceFragment()) {
                    Fragment attendanceFragment = new ActivityAttendance();
                    fragmentTransaction.replace(R.id.phone_layout, attendanceFragment);
                    Bundle bundleDetail = new Bundle();
                    bundleDetail.putParcelable("data", saveItem);
                    attendanceFragment.setArguments(bundleDetail);
                } else {
                    Fragment subjectDetailFragment = new ActivitySubjectDetail();
                    fragmentTransaction.replace(R.id.phone_layout, subjectDetailFragment);
                    Bundle bundleDetail = new Bundle();
                    bundleDetail.putParcelable("data", saveItem);
                    subjectDetailFragment.setArguments(bundleDetail);
                }
            }
        }

        fragmentTransaction.commit();

    }

    private void colorPicker() {

        fab.hide();
        layoutInflater = LayoutInflater.from(context);
        promptsView = layoutInflater.inflate(R.layout.color_picker, null);
        saveItem.setPopupInflated(false);
        saveItem.setColorPopupInflated(true);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (picker.getColor() == 0 || picker.getColor() == -1)
                                    saveItem.setIntSelectedColor(-1447447);
                                else
                                    saveItem.setIntSelectedColor(picker.getColor());

                                saveItem.setColorPopupInflated(false);
                                saveItem.setPopupInflated(true);
                                layoutInflater = null;
                                promptsView = null;
                                fabClicked();
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                saveItem.setColorPopupInflated(false);
                                saveItem.setPopupInflated(true);
                                layoutInflater = null;
                                promptsView = null;
                                fabClicked();
                            }
                        });

        try {
            alertDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        picker = (ColorPicker) promptsView.findViewById(R.id.picker);
        SVBar svBar = (SVBar) promptsView.findViewById(R.id.sv_bar);
        OpacityBar opacityBar = (OpacityBar) promptsView.findViewById(R.id.opacity_bar);
        SaturationBar saturationBar = (SaturationBar) promptsView.findViewById(R.id.saturation_bar);
        ValueBar valueBar = (ValueBar) promptsView.findViewById(R.id.value_bar);

        picker.addSVBar(svBar);
        picker.addOpacityBar(opacityBar);
        picker.addSaturationBar(saturationBar);
        picker.addValueBar(valueBar);

        picker.getColor();

        picker.setOldCenterColor(saveItem.getIntSelectedColor());

        picker.setShowOldCenterColor(false);

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(ContextCompat.getColor(context, R.color.greenColoredButton));

        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(ContextCompat.getColor(context, R.color.redColoredButton));

    }

    private void fabClicked() {

        fab.hide();
        layoutInflater = LayoutInflater.from(context);
        promptsView = layoutInflater.inflate(R.layout.popup, null);
        saveItem.setPopupInflated(true);
        saveItem.setColorPopupInflated(false);

        TextView selectedColorImage = (TextView) promptsView.findViewById(R.id.selected_color);
        selectedColorImage.setBackgroundColor(saveItem.getIntSelectedColor());

        subjectName = (EditText) promptsView.findViewById(R.id.subject_name);
        passPercentage = (EditText) promptsView.findViewById(R.id.pass_per);

        subjectName.setText(saveItem.getSubName());

        passPercentage.setText(saveItem.getPassPer());


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView)
                .setTitle("Add New Subject")
                .setCancelable(false)
                .setPositiveButton("ADD",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if (saveItem.getIntSelectedColor() == 0 || saveItem.getIntSelectedColor() == -1)
                                    saveItem.setIntSelectedColor(-1447447);

                                saveItem.setSubName(subjectName.getText().toString());
                                saveItem.setPassPer(passPercentage.getText().toString());
                                if (saveItem.getSubName() == null || saveItem.getSubName().equals("") || saveItem.getPassPer() == null || saveItem.getPassPer().equals("")) {
                                    Toast.makeText(context, "Enter all the fields", Toast.LENGTH_SHORT).show();
                                    layoutInflater = null;
                                    promptsView = null;
                                    fabClicked();
                                } else {
                                    try {
                                        Subjects subject = new Subjects(
                                                null,
                                                subjectName.getText().toString(),
                                                Float.parseFloat(passPercentage.getText().toString()),
                                                saveItem.getIntSelectedColor(),
                                                00.00f,
                                                0,
                                                0,
                                                0,
                                                0,
                                                saveItem.getCurrentUserID()
                                        );


                                        try {
                                            newSubjectID = subjectsDao.insertOrReplace(subject);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                        showSnackBar("New Subject Added");
                                        layoutInflater = null;
                                        promptsView = null;
                                        saveItem.setPopupInflated(false);
                                        saveItem.setColorPopupInflated(false);
                                        fab.show();
                                        fragmentTransaction();

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                saveItem.setPopupInflated(false);
                                saveItem.setColorPopupInflated(false);
                                layoutInflater = null;
                                promptsView = null;
                                fab.show();
                                alertDialog.dismiss();
                            }
                        });

        try {
            alertDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        TextView colorPickerButton = (TextView) promptsView.findViewById(R.id.selected_color);
        colorPickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(promptsView.getWindowToken(), 0);
                layoutInflater = null;
                promptsView = null;
                saveItem.setPopupInflated(false);
                saveItem.setColorPopupInflated(true);
                alertDialog.dismiss();
                saveItem.setSubName(subjectName.getText().toString());
                saveItem.setPassPer(passPercentage.getText().toString());
                colorPicker();
            }
        });

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setTextColor(ContextCompat.getColor(context, R.color.greenColoredButton));

        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeButton.setTextColor(ContextCompat.getColor(context, R.color.redColoredButton));

    }

    private void showSnackBar(String msg) {
        Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_SHORT)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        subjectsDao.deleteByKey(newSubjectID);
                        fragmentTransaction();
                    }
                })
                .setActionTextColor(ContextCompat.getColor(context, R.color.yellowColor))
                .show();
    }

    private void datePicker() {

        saveItem.setDatePickerInflated(true);

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int newYear, int monthOfYear, int dayOfMonth) {
                saveItem.setDatePickerInflated(false);
                saveItem.setYear(newYear);
                saveItem.setMonth(monthOfYear + 1);
                saveItem.setDay(dayOfMonth);
                String str = findMonth(saveItem.getMonth()) + " " + saveItem.getDay() + ", " + saveItem.getYear();
                datePickerButton.setText(str);
            }
        };
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, datePickerListener, saveItem.getYear(), saveItem.getMonth(), saveItem.getDay());

        datePickerDialog.setCanceledOnTouchOutside(false);
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        saveItem.setDatePickerInflated(false);
                        saveItem.setMonth(saveItem.getMonth() + 1);
                        saveItem.setMonthString(findMonth(saveItem.getMonth()));
                        dialog.cancel();
                    }
                });
        datePickerDialog.show();


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        try {
            saveItem.setSubName(subjectName.getText().toString());
            saveItem.setPassPer(passPercentage.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        outState.putParcelable("savedItems", saveItem);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        saveItem = savedInstanceState.getParcelable("savedItems");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE) {
            if (data != null) {
                try {
                    Uri selectedImageURI = data.getData();
                    profilePicture.setImageURI(selectedImageURI);

                    long previousUserId = settings.getLong(CURRENT_USER_ID, 0);
                    Users currentUser = usersDao.load(previousUserId);
                    currentUser.setImage_uri(String.valueOf(selectedImageURI));
                    usersDao.delete(currentUser);
                    usersDao.insert(currentUser);

                    SharedPreferences.Editor newSettings = settings.edit();
                    newSettings.putBoolean(PROFILE_PIC_ADDED, true);
                    newSettings.apply();
                    try {
                        alertDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
