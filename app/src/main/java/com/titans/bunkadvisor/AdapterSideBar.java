package com.titans.bunkadvisor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterSideBar extends ArrayAdapter<String> {


    private final Context context;
    private final String[] sideListItems;

    public AdapterSideBar(Context context, String[] sideListItems) {
        super(context, R.layout.sidebar_list_items, sideListItems);
        this.context = context;
        this.sideListItems = sideListItems;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rowView = inflater.inflate(R.layout.sidebar_list_items, null, true);

        TextView title = (TextView) rowView.findViewById(R.id.item_name);
        ImageView itemImage = (ImageView) rowView.findViewById(R.id.icon_image);

        title.setText(sideListItems[position]);
        switch (position) {
            case 0:
                itemImage.setImageResource(R.mipmap.add_user_icon);
                break;
            case 1:
                itemImage.setImageResource(R.mipmap.switch_user_icon);
                break;
            case 2:
                itemImage.setImageResource(R.mipmap.delete_icon);
                break;
            case 3:
                itemImage.setImageResource(R.mipmap.settings_icon);
                break;
            case 4:
                itemImage.setImageResource(R.mipmap.version_icon);
                String string = title.getText() + context.getResources().getString(R.string.version);
                title.setText(string);
                break;
        }
        return rowView;

    }
}

