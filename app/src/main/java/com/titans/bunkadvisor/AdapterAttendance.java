package com.titans.bunkadvisor;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.titans.bunkadvisor.model.Attendance;

import java.util.List;

public class AdapterAttendance extends RecyclerView.Adapter<AdapterAttendance.ViewHolder> {

    private List<Attendance> attendanceList;
    private Attendance attendance;
    private Context context;
    private LayoutInflater inflater;
    private View AttendanceView;
    private ButtonClickListener buttonClickListener;

    public AdapterAttendance(List<Attendance> attendanceList, Context context) {
        this.attendanceList = attendanceList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView status, date;
        public ImageButton dropDownButton;

        public ViewHolder(View v) {
            super(v);

            status = (TextView) v.findViewById(R.id.status);
            date = (TextView) v.findViewById(R.id.date);

            dropDownButton = (ImageButton) v.findViewById(R.id.attendance_item_button);

            dropDownButton.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (buttonClickListener != null)
                buttonClickListener.buttonClicked(view, getLayoutPosition());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AttendanceView = inflater.inflate(R.layout.attendance_item, parent, false);
        return new ViewHolder(AttendanceView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        attendance = attendanceList.get(position);

        holder.status.setText(attendance.getStatus());
        if (attendance.getStatus().equals("Attended")) {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.greenColoredButton));
        } else if (attendance.getStatus().equals("Bunked")) {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.redColoredButton));
        }

        holder.date.setText(attendance.getDate());

    }

    @Override
    public int getItemCount() {
        if (attendanceList != null)
            return attendanceList.size();
        else
            return 0;
    }

    public interface ButtonClickListener {
        void buttonClicked(View v, int position);
    }

    public void setClickListener(ButtonClickListener buttonClickListener) {
        this.buttonClickListener = buttonClickListener;
    }
}
