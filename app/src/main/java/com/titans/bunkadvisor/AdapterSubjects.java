package com.titans.bunkadvisor;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.titans.bunkadvisor.model.Subjects;

import java.util.List;

public class AdapterSubjects extends RecyclerView.Adapter<AdapterSubjects.ViewHolder> {
    private List<Subjects> subjectsList;
    private Context context;
    private LayoutInflater inflater;
    private ClickListener clickListener;
    private AttendedClickListener attendedClickListener;
    private BunkedClickListener bunkedClickListener;
    private View SubjectView;
    private String getString, formattedString;
    private Subjects subjects;

    public AdapterSubjects(List<Subjects> subjectsList, Context context) {
        this.subjectsList = subjectsList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView subjectName, safeBunks, curPer;
        public Button attendedButton, bunkedButton;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            subjectName = (TextView) v.findViewById(R.id.sub_name);
            safeBunks = (TextView) v.findViewById(R.id.safe_bunks);
            curPer = (TextView) v.findViewById(R.id.cur_per);

            attendedButton = (Button) v.findViewById(R.id.attended_button);
            bunkedButton = (Button) v.findViewById(R.id.bunked_button);

            attendedButton.setTextColor(ContextCompat.getColor(context, R.color.greenColoredButton));
            bunkedButton.setTextColor(ContextCompat.getColor(context, R.color.redColoredButton));

            attendedButton.setOnClickListener(this);

            bunkedButton.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int id = view.getId();

            switch (id) {
                case R.id.card_view:
                    if (clickListener != null)
                        clickListener.itemClicked(view, getLayoutPosition());
                    break;
                case R.id.attended_button:
                    if (attendedClickListener != null)
                        attendedClickListener.attendedClicked(getLayoutPosition());
                    break;
                case R.id.bunked_button:
                    if (bunkedClickListener != null)
                        bunkedClickListener.bunkedClicked(getLayoutPosition());
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SubjectView = inflater.inflate(R.layout.subject_card, parent, false);
        return new ViewHolder(SubjectView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        subjects = subjectsList.get(position);

        holder.subjectName.setText(subjects.getSub_name());
        int safeBunks = subjects.getSafe_bunks();
        int length;
        SpannableStringBuilder spannableStringBuilder;
        if (safeBunks < 0) {
            length = subjects.getSafe_bunks().toString().length();
            getString = context.getResources().getString(R.string.attend_classes);
            formattedString = String.format(getString, (safeBunks * -1), subjects.getPass_per());
            formattedString = formattedString + " %";

            spannableStringBuilder = new SpannableStringBuilder(formattedString);
            spannableStringBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD),
                    19,
                    19 + length - 1,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
            );
        } else {
            getString = context.getResources().getString(R.string.safe_bunks);
            formattedString = String.format(getString, safeBunks);
            length = getString.length();
            spannableStringBuilder = new SpannableStringBuilder(formattedString);
            spannableStringBuilder.setSpan(
                    new StyleSpan(Typeface.BOLD),
                    21,
                    length - 1,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
            );
        }
        holder.safeBunks.setText(spannableStringBuilder);

        String string;
        int len;
        try {
            string = subjects.getCur_per().toString().substring(0, 5) + " %";
            len = 5;
        } catch (Exception e) {
            try {
                string = subjects.getCur_per().toString().substring(0, 4) + " %";
                len = 4;
            } catch (Exception e1) {
                string = subjects.getCur_per().toString().substring(0, 3) + " %";
                len = 3;
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        spannableStringBuilder = new SpannableStringBuilder(string);

        if (subjects.getCur_per() < subjects.getPass_per()) {
            spannableStringBuilder.setSpan(
                    new ForegroundColorSpan(ContextCompat.getColor(context, R.color.redColor)),
                    0,
                    len,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
            );
        } else {
            spannableStringBuilder.setSpan(
                    new ForegroundColorSpan(ContextCompat.getColor(context, R.color.greenColor)),
                    0,
                    len,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
            );
        }

        holder.curPer.setText(spannableStringBuilder);

        holder.subjectName.setTextColor(subjects.getTheme());

        holder.subjectName.setShadowLayer(1.6f, 1.5f, 1.3f, ContextCompat.getColor(context, R.color.colorPrimaryText));

    }

    @Override
    public int getItemCount() {
        if (subjectsList != null)
            return subjectsList.size();
        else
            return 0;
    }

    public interface ClickListener {
        void itemClicked(View v, int position);
    }

    public interface AttendedClickListener {
        void attendedClicked(int position);
    }

    public interface BunkedClickListener {
        void bunkedClicked(int position);
    }

    public void setAttendedClickListener(AttendedClickListener attendedClickListener) {
        this.attendedClickListener = attendedClickListener;
    }

    public void setBunkedClickListener(BunkedClickListener bunkedClickListener) {
        this.bunkedClickListener = bunkedClickListener;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }
}
