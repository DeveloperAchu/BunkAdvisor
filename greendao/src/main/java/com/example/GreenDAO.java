package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class GreenDAO {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "com.titans.bunkadvisor.model");

        Entity users = schema.addEntity("Users");

        users.addIdProperty();
        users.addStringProperty("user_name").notNull();
        users.addStringProperty("image_uri");

        Entity subjects = schema.addEntity("Subjects");

        subjects.addIdProperty();
        subjects.addStringProperty("sub_name").notNull();
        subjects.addFloatProperty("pass_per").notNull();
        subjects.addIntProperty("theme");
        subjects.addFloatProperty("cur_per");
        subjects.addIntProperty("attend");
        subjects.addIntProperty("bunk");
        subjects.addIntProperty("total");
        subjects.addIntProperty("safe_bunks");

        Property subjectsForUser = subjects.addLongProperty("User_id").notNull().getProperty();
        subjects.addToOne(users, subjectsForUser);
        users.addToMany(subjects,subjectsForUser);

        Entity attendances = schema.addEntity("Attendance");

        attendances.addIdProperty();
        attendances.addStringProperty("status").notNull();
        attendances.addStringProperty("date").notNull();
        attendances.addStringProperty("actualDate").notNull();

        Property attendanceForSubject = attendances.addLongProperty("Subjects_id").notNull().getProperty();
        attendances.addToOne(subjects, attendanceForSubject);
        subjects.addToMany(attendances,attendanceForSubject);

        new DaoGenerator().generateAll(schema, "../app/src/main/java");
    }
}